const express = require('express');
const mongoose = require('mongoose');

require('dotenv').config();


// CORS Middleware
const cors = require('cors');
const { initialRole } = require('./app/models/data');

const app = express();

const port = process.env.SERVER_PORT || 8000;

app.use(cors());
app.use(express.json());

mongoose.connect(process.env.MONGODB_URL)
    .then(() => {
        console.log("Connect successfully!");
        initialRole();
    })
    .catch((error) => {
        console.error("Connect MongoDB Failed!");
    })

app.get("/", (req, res) => {
    res.json({
        message: "Welcome to Devcamp JWT"
    })
})

// Viết tắt
app.use("/api/auth/", require("./app/routes/auth.routes"));
app.use("/api/user/", require("./app/routes/user.routes"));

app.listen(port, () => {
    console.log("App listening on port", port);
})