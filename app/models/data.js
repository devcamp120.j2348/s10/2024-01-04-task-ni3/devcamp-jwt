// Dữ liệu role sẽ được sinh sẵn tại đây
const roleModel = require("./role.model");

const initialRole = async () => {
    const countRole = await roleModel.estimatedDocumentCount();

    if (countRole === 0) {
        await new roleModel({
            name: "Admin"
        }).save();
        await new roleModel({
            name: "User"
        }).save();
        await new roleModel({
            name: "Guest"
        }).save();
    }
}

module.exports = {
    initialRole
}