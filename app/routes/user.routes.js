const express = require("express");

const route = express.Router();

const userController = require("../controllers/user.controller");
const userMiddleWare = require("../middlewares/user.middleware")

route.get('/', userMiddleWare.verifyToken, userController.getAllUser);
route.post('/', [userMiddleWare.verifyToken, userMiddleWare.checkUser], userController.createUser);
route.get('/:userid', userController.getUserById);
route.put('/:userid', userController.updateUser);
route.delete('/:userid', userController.deleteUser);

module.exports = route;