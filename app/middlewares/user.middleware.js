const jwt = require("jsonwebtoken");
const userModel = require("../models/user.model");

const verifyToken = async (req, res, next) => {
    try {
        console.log('verify token ...');
        let token = req.headers['x-access-token'];
        console.log(token);
        if (!token) {
            return res.status(401).json({
                message:"Token not found!"
            })
        }
    
        const secretKey = process.env.JWT_SECRET;
    
        const verified = jwt.verify(token, secretKey);
        console.log(verified);
        if (!verified) {
            return res.status(401).json({
                message:"Token invalid!"
            })
        }
    
        const user = await userModel.findById(verified.id).populate("roles");
        console.log(user);
        req.user = user;
        
        next();
    } catch (error) {
        console.error(error);
        return res.status(500).json({
            message: "Interal server error"
        })
    }   
}


const checkUser = async (req, res, next) => {
    try {
        console.log('check user ...');
        const userRoles = req.user.roles;
        console.log(req.user);
        if (userRoles) {
            for (let i=0; i<userRoles.length; i++) {
                console.log(userRoles[i].name);
                if (userRoles[i].name == 'Admin') {
                    console.log('authorized!');
                    next();
                    return;
                }
            }
        }
        console.log('unauthorized!');
        return res.status(401).json({
            message:"Unauthorized!"
        })
    } catch (error) {
        console.error(error);
        return res.status(500).json({
            message: "Interal server error"
        })
    }              
}

module.exports = {
    verifyToken,
    checkUser 
}