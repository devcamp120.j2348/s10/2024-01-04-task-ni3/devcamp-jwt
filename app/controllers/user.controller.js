
const getAllUser = (req, res) => {
    res.status(200).json({
        message:"Get All Users"
    })
}

const getUserById = (req, res) => {
    res.status(200).json({
        message:"Get a User"
    })
}

const createUser = (req, res) => {
    res.status(201).json({
        message:"Create new User"
    })
}

const updateUser = (req, res) => {
    res.status(200).json({
        message:"Update a User"
    })
}

const deleteUser = (req, res) => {
    res.status(200).json({
        message:"Delete a User"
    })
}

module.exports = {
    getAllUser, getUserById, createUser, updateUser, deleteUser
}