const roleModel = require("../models/role.model");
const userModel = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const signUp = async (req, res) => {
    try {
        console.log(req.body);
        const { username, email, role, password } = req.body;

        const existedAccount = await userModel.findOne({ username: username });

        if(existedAccount) {
            return res.status(400).json({
                message: "Username existed"
            })
        }

        const userRole = await roleModel.findOne({ name: role });
        console.log(userRole);
        const user = new userModel({
            username: username,
            email: email,
            password: bcrypt.hashSync(password, 8),
            roles: [
                userRole._id
            ]
        }).save();

        res.status(200).json({
            message: "Create user successfully"
        })

    } catch (error) {
        console.error(error);
        res.status(500).json({
            message: "Interal server error"
        })
    }
}

const signIn = async (req, res) => {
    try {
        const {username, password} = req.body;

        const existedAccount = await userModel.findOne({username: username});

        if(!existedAccount) {
            return res.status(404).json({
                message: "User not found"
            })
        }

        // Hàm so sánh giá trị mật khẩu
        var passwordIsValid = bcrypt.compareSync(
            password,
            existedAccount.password
        )

        if(!passwordIsValid) {
            return res.status(401).json({
                message: "Invalid password"
            })
        }

        const secretKey = process.env.JWT_SECRET;
        const expiresTime = process.env.JWT_EXPIRES;

        const accessToken = jwt.sign(
            {
                id: existedAccount._id
            },
            secretKey,
            {
                algorithm: "HS256",
                allowInsecureKeySizes: true,
                expiresIn: expiresTime
            }
        )

        return res.status(200).json({
            accessToken: accessToken
        })
    } catch (error) {
        console.error(error);
        res.status(500).json({
            message: "Interal server error"
        })
    }
}

module.exports = {
    signUp,
    signIn
}